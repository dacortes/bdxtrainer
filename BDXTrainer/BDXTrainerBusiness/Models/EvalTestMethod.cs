﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BDXTrainerBusiness;
using KellermanSoftware.CompareNetObjects;

namespace BDXTrainer
{
    public class EvalTestMethod
    {
        public string Name; // Method name
        public EvalTestType Class; // Reference to the method class
        public EvalTestVar Instance; // Variable to use as Instance
        public List<EvalTestVar> Params; // Variables to use as Parameters
        public EvalTestVar Expect; // Variable to compare with the result of the method

        public EvalTestMethod(string name, EvalTestType fromClass)
        {
            Name = name;
            Class = fromClass;
        }
         
        /// <summary>
        ///     Runs a void method
        /// </summary>
        /// <param name="method">
        ///     Method to run
        /// </param>
        /// <returns>
        ///     If the method ran successfully
        /// </returns>
        private bool RunMethod(MethodInfo method)
        {
            try
            {
                if (Params == null)
                {
                    if (Instance != null)
                    {
                        method.Run(Instance.Value);
                    }
                    else
                    {
                        method.Run();
                    }
                }
                else
                {
                    if (Instance != null)
                    {
                        method.Run(Instance.Value, Params.Select(p => p.Value).ToArray());
                    }
                    else
                    {
                        method.Run(Params.Select(p => p.Value).ToArray());
                    }
                }
            }
            catch (Exception error)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Runs a method and returns its result
        /// </summary>
        /// <param name="method">Method reference</param>
        /// <returns>Method result</returns>
        private object GetMethodResults(MethodInfo method)
        {
            if (Params == null)
            {
                return Instance != null ? method.Run(Instance.Value) : method.Run();
            }

            return Instance != null ? method.Run(Instance.Value, Params.Select(p => p.Value).ToArray()) : method.Run(Params.Select(p => p.Value).ToArray());
        }

        /// <summary>
        /// Runs the method and according to the set properties
        /// </summary>
        /// <returns>If the action success according to the properties</returns>
        public bool Run()
        {
            var method = Class.RealType.FindMethod(Name);

            if (method == null)
            {
                throw new Exception("The method " + Name + " was not found in Type " + Class.RealType.Name);
            }

            if (Expect == null) // No expect value assigned so Run as void
            {
                return RunMethod(method);
            }

            // Expect value has been assigned so get results and compare against expected value
            var compareLogic = new CompareLogic();
            var result = compareLogic.Compare(Expect.Value, GetMethodResults(method));
            return result.AreEqual;
        }

    }
}
