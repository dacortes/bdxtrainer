﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDXTrainer
{
    public class EvalTestVar
    {
        /// <summary>
        /// Variable Type reference
        /// </summary>
        public EvalTestType TestType; 
        
        /// <summary>
        /// Default value to use, this can only be used when the value can be converted from a string
        /// </summary>
        public string TestValue;

        private dynamic _value;

        /// <summary>
        /// Variable real value
        /// </summary>
        public object Value => _value;

        public EvalTestVar(EvalTestType type, string testValue = "")
        {
            TestType = type;
            TestValue = testValue;
            CreateVariable();
            InitializeVariable();
        }

        /// <summary>
        /// Creates the variable in memory
        /// </summary>
        private void CreateVariable()
        {
            if (TestType.ConstructorParams.Count == 0)
            {
                _value = Activator.CreateInstance(TestType.RealType);
            }
            else
            {             
                var constructorParams = TestType.ConstructorParams.Select(v => v.Value).ToArray();
                _value =  Activator.CreateInstance(TestType.RealType, constructorParams);
            }             
        }

        /// <summary>
        /// Assigns a value to the variable
        /// </summary>
        private void InitializeVariable()
        {
            if (TestValue == string.Empty) return;
            _value = Convert.ChangeType(TestValue, TestType.RealType);
        }

               
    }
}
