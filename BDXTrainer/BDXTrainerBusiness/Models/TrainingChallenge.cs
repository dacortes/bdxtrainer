﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using BDXTrainerBusiness;

namespace BDXTrainer.Models
{
    public class TrainingChallenge
    {        
        public String Title { get; set; }
        public String Description { get; set; }
        public TimeSpan EstimatedTime { get; set; }
        public XElement Tests;        

        public TrainingChallenge(XElement challenge)
        {
            Title = challenge.Element("title")?.Value ?? throw new Exception("invalid Title");

            Description = challenge.Element("description")?.Value ?? throw new Exception("invalid Description");

            Tests = challenge.Element("tests");
        }        
    }
}
