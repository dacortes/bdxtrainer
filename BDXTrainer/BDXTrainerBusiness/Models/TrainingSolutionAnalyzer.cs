﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using BDXTrainerBusiness;

namespace BDXTrainer.Models
{
    public  class TrainingSolutionAnalyzer
    {
        public CSharpEval EvalContext;
        public Dictionary<string, EvalTestType> Types = new Dictionary<string, EvalTestType>();
        public List<EvalTest> Tests = new List<EvalTest>();

        public TrainingSolutionAnalyzer(CSharpEval evalContext, TrainingChallenge trainingChallenge)
        {
            EvalContext = evalContext;                        
            SetUpTypes(XElement.Load(AppDomain.CurrentDomain.BaseDirectory + "/../../../TestTypes.xml"));
            SetUpTests(trainingChallenge.Tests);
        }

        private void SetUpTests(XElement testsNode)
        {
            foreach (var testNode in testsNode.Elements("test"))
            {
                var descriptionAttr = testNode.Attribute("description");
                if (descriptionAttr == null) throw new Exception("Missing description attribute in test " + testNode);
                var description = descriptionAttr.Value;
                var methodNode = testNode.Element("method");
                if (methodNode == null) throw new Exception("Missing method in test" + testNode);
                var method = SetUpMethod(methodNode);

                Tests.Add(new EvalTest()
                {
                    Description = description,
                    Method = method,
                    Types = Types
                });
            }
        }

        /// <summary>
        /// Creates the types defined in the test
        /// </summary>
        /// <param name="testNode">test node where types are defined</param>
        private void SetUpTypes(XElement testNode)
        {            
            if (testNode == null) return;
            var types = testNode.Elements("type").ToList();
            foreach (var typeNode in types)
            {
                var nameSpaceAttr = typeNode.Attribute("namespace");
                if (nameSpaceAttr == null) throw new Exception("Missing namespace attribute in type definition " + typeNode);
                var nameAttr = typeNode.Attribute("name");
                if (nameAttr == null) throw new Exception("Missing name attribute in type definition " + typeNode);
                var genericAttr = typeNode.Attribute("generic");
                var type = SetUpType(nameSpaceAttr.Value, nameAttr.Value, genericAttr?.Value.Split(',').ToList().Select(FindType).ToList());
                SetUpConstructor(typeNode, type);
                Types.Add(type.Name, type);
            }
        }

        /// <summary>
        /// Creates a type using the desired basic setup
        /// </summary>
        /// <param name="nameSpace">Namespace to use</param>
        /// <param name="name">Name to use in the Type</param>
        /// <param name="generic">Optional* Generic types to assign to the Type Generics</param>
        /// <returns></returns>
        private  EvalTestType SetUpType(string nameSpace, string name, List<EvalTestType> generic = null)
        {
            return generic == null ? new EvalTestType(EvalContext, nameSpace, name) : new EvalTestType(EvalContext, nameSpace, name, generic);
        }

        /// <summary>
        /// Configures a default constructor for all the instances of a Type
        /// </summary>
        /// <param name="typeNode">Type node where the constructor is defined</param>
        /// <param name="type">Type instance</param>
        private  void SetUpConstructor(XElement typeNode, EvalTestType type)
        {
            var constructorNode = typeNode.Element("constructor");
            if (constructorNode == null) return;
            var constructorParamsNodes = constructorNode.Elements("var").ToList();
            if (constructorParamsNodes.Count == 0) throw new Exception("Invalid type constructor parameters");
            type.ConstructorParams = constructorParamsNodes.Select(CreateVariable).ToList();
        }

        /// <summary>
        /// Creates a variable from an XML var tag
        /// </summary>
        /// <param name="variableNode"></param>
        /// <returns></returns>
        private  EvalTestVar CreateVariable(XElement variableNode)
        {
            var typeAttr = variableNode.Attribute("type");
            var valAttr = variableNode.Attribute("value");
            if (typeAttr == null) throw new Exception("Missing type attribute in variable definition " + variableNode);

            var variable = new EvalTestVar(FindType(typeAttr.Value), valAttr != null ? valAttr.Value : string.Empty);

            var methodNodes = variableNode.Elements("method").ToList();
            if (methodNodes.Count > 0) ApplyVariableMethods(methodNodes, variable);

            return variable;
        }

        /// <summary>
        /// Creates a method from an XML method tag
        /// </summary>
        /// <param name="methodNode">Method Node to SetUp</param>
        /// <returns>Method created</returns>
        private EvalTestMethod SetUpMethod(XElement methodNode, EvalTestVar instance = null)
        {
            // Get Method Name
            var nameAttr = methodNode.Attribute("name");
            if (nameAttr == null) throw new Exception("Missing name attribute in test method " + methodNode);

            // Get Method Class
            var classAttr = methodNode.Attribute("class");
            if (classAttr == null && instance == null) throw new Exception("Missing class attribute in test method " + methodNode);

            var classType = instance == null ? FindType(classAttr.Value) : instance.TestType;

            // Create Method
            var method = new EvalTestMethod(nameAttr.Value, classType);

            if (instance != null) method.Instance = instance;

            // If <params> assign them to the Method 
            var paramsNode = methodNode.Element("parameters");
            if (paramsNode != null || instance != null)
            {
                var paramsContainer = paramsNode ?? methodNode;
                if (paramsContainer.HasElements)
                {
                    var varNodes = paramsContainer.Elements("var").ToList();
                    if (varNodes.Count == 0) throw new Exception("Can not find any variable inside the parameters for invoking test method " + methodNode);
                    method.Params = varNodes.Select(CreateVariable).ToList();
                }
            }

            // If <expect> assign it to the Method
            var expectNode = methodNode.Element("expect");
            if (expectNode != null)
            {
                var varNode = expectNode.Element("var");
                if (varNode == null) throw new Exception("Missing variable inside method expect definition " + methodNode);
                method.Expect = CreateVariable(varNode);
            }

            return method;
        }
        

        /// <summary>
        /// Applies all the methods from the instance of a variable declared using XML var tag
        /// </summary>
        /// <param name="methodNodes"></param>
        /// <param name="variable"></param>
        private void ApplyVariableMethods(List<XElement> methodNodes, EvalTestVar variable)
        {
            methodNodes.ForEach(mn => { SetUpMethod(mn, variable).Run(); });
        }

        private List<string> SplitGenerics(string genericField)
        {
            var defaultResult = new List<string> { genericField };
            if (!genericField.Contains(',')) return defaultResult;
            var genericFieldBuilder = new StringBuilder(genericField);
            var openedParentheses = 0;
            for (var index = 0; index < genericFieldBuilder.Length; index++)
            {
                var c = genericFieldBuilder[index];
                switch (c)
                {
                    case '(':
                        openedParentheses++;
                        break;
                    case ')':
                        openedParentheses--;
                        break;
                    case ',' when openedParentheses == 0:
                        genericFieldBuilder.Replace(c, '<', index, 1);
                        break;
                }
            }
            genericField = genericFieldBuilder.ToString();
            return !genericField.Contains('<') ? defaultResult : genericField.Split('<').ToList();
        }

        private EvalTestType CreateType(string typeField)
        {
            if (Types.ContainsKey(typeField)) return FindType(typeField);
            if (!typeField.Contains('(')) return SetUpType(typeField, typeField);
            var typeName = typeField.Split('(')[0];
            var type = FindType(typeName);
            typeField = Regex.Match(typeField, "(?<=\\().+(?=\\))").Value;
            var genericNames = SplitGenerics(typeField);
            var genericTypes = genericNames.Select(g => CreateType(g).RealType).ToArray();
            return new EvalTestType(type.RealType.MakeGenericType(genericTypes), typeField);
        }

        private EvalTestType FindType(string name)
        {
            if (Types.ContainsKey(name)) return Types[name];
            var newType = CreateType(name);
            Types.Add(name, newType);
            return newType;
        }

        public bool Test()
        {
            var failure = false;
            foreach (var t in Tests)
            {
                if (!t.Run())
                {
                    Console.WriteLine("Test: "+t.Description+" FAILED");
                    failure = true;
                }
                else
                {
                    Console.WriteLine("Test: " + t.Description + " PASSED");
                }
            }
            return !failure;
        }

    }
}
