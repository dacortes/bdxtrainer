﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDXTrainerBusiness;

namespace BDXTrainer
{
    public class EvalTestType
    {
        /// <summary>
        /// Eval context to use
        /// </summary>
        public CSharpEval EvalContext; 

        /// <summary>
        /// Type's namespace
        /// </summary>
        public string Namespace;

        /// <summary>
        /// Type's reference name
        /// </summary>
        public string Name;

        /// <summary>
        /// Real C# System type reference
        /// </summary>
        public Type RealType;

        /// <summary>
        /// Generic values to use in Type
        /// </summary>
        public List<EvalTestType> Generics;

        /// <summary>
        /// Parameters to use in variable default constructor
        /// </summary>
        public List<EvalTestVar> ConstructorParams = new List<EvalTestVar>();

        public EvalTestType(Type type, string name)
        {
            Namespace = type.Namespace;
            Name = name;
            RealType = type;
        }

        public EvalTestType(CSharpEval context, string nameSpace, string name)
        {
            BaseInitialize(context, nameSpace, name);
        }

        public EvalTestType(CSharpEval context, string nameSpace, string name, List<EvalTestType> generic)
        {
            BaseInitialize(context, nameSpace, name);
            Generics = generic;
            ApplyGeneric();
        }


        /// <summary>
        /// Initializes the instance properties and
        /// looks for the desired type in the Eval context    
        /// </summary>
        /// <param name="context">Eval context to use</param>
        /// <param name="nameSpace">Namespace to find</param>
        /// <param name="name">Reference name to use</param>
        private void BaseInitialize(CSharpEval context, string nameSpace, string name)
        {
            EvalContext = context;
            Namespace = nameSpace;
            Name = name;
            RealType = EvalContext.FindType(Namespace);

            if (RealType == null)
            {
                throw new Exception("Can not find Type in namespace " + nameSpace);
            }
        }

        /// <summary>
        /// Applies the Generics types to the generics of the C# System Type instance
        /// </summary>
        private void ApplyGeneric()
        {
            var generics = Generics.Select(t => t.RealType).ToArray();
            RealType = RealType.MakeGenericType(generics);
        }
    }
}
