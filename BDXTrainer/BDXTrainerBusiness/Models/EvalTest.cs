﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using BDXTrainerBusiness;

namespace BDXTrainer
{
    public class EvalTest
    {
       
        public string Description;
        public Dictionary<string, EvalTestType> Types;
        public EvalTestMethod Method;

        /// <summary>
        /// Runs the Test and retrieve the results
        /// </summary>
        /// <returns>Test result</returns>
        public bool Run()
        {
            try
            {
                return Method.Run();
            }
            catch (Exception)
            {
                //Console.WriteLine(error);
                return false;
            }
        }
    }
}
