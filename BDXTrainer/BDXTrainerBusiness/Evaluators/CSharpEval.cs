﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;


namespace BDXTrainerBusiness
{
    public class CSharpEval
    {
        private readonly Dictionary<String, Type> _dllTypes;
        private readonly string _code;
        private readonly Assembly _dll;


        public CSharpEval(string code)
        {
            _code = code;
            _dll = Compile();
            _dllTypes = BuildTypeDictionary();
        }   

        private Assembly Compile()
        {
            var memoryStream = new MemoryStream();
            CSharpCompilationOptions compileOptions = new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary);
            CSharpCompilation compilation = CSharpCompilation.Create("InMemoryAssembly").WithOptions(compileOptions).AddReferences(MetadataReference.CreateFromFile(typeof(object).Assembly.Location));
            var tree = CSharpSyntaxTree.ParseText(_code);
            var newCompilation = compilation.AddSyntaxTrees(tree);
            var emitResult = newCompilation.Emit(memoryStream);
            if (emitResult.Success)
                return Assembly.Load(memoryStream.ToArray()); //return the assembly that is in the memory stream
            throw new InvalidOperationException("Can't compile dll");
        }

        private Dictionary<string, Type> BuildTypeDictionary()
        {
            try
            {
                var types = _dll.GetExportedTypes();                
                return types.ToDictionary(t => t.Name, t => t);
            }
            catch (Exception)
            {
                return new Dictionary<string, Type>();
            }
        }

        public Type FindType(string className)
        {
            Type type;
            try
            {
                type = _dllTypes[className];
            }
            catch (Exception)
            {
                type = Type.GetType(className);

            }
            return type;
        }



    }
}
