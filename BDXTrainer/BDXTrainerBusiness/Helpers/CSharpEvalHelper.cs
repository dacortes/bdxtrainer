﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BDXTrainerBusiness
{
    public static class CSharpEvalHelper
    {

        private static object RunMethod(MethodInfo method, object[] args = null, object instance = null)
        {
            return method.Invoke(instance, args == null ? new object[] { } : args);
        }

        public static object Run(this MethodInfo method, object[] args = null)
        {
            return RunMethod(method, args);
        }

        public static object Run(this MethodInfo method, object instance, object[] args = null)
        {
            return RunMethod(method, args, instance);
        }

        public static MethodInfo FindMethod(this Type classType, string methodName)
        {
            return classType.GetMethod(methodName);
        }

    }
}
