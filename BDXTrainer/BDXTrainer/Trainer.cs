﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDXTrainer
{
    class Trainer
    {
        public TrainingCommands Commands;
        public Trainer()
        {
            Commands = new TrainingCommands();
            RegisterCommands();
        }

        private void OnChallengeRequest(List<string> options)
        {
            Show("You requested a challenge");
            Show("Loading...");
            Show("We do not have challenges registered yet, sorry");
        }
        private void OnSolution(List<string> options)
        {
            var solutionPath = options[0];
            if (!string.IsNullOrEmpty(solutionPath))
            {
                Show("You gave us the following solution: " + solutionPath);
            }
            else
            {
                Show("Please include a solution path");
            }             
           
        }

        private void OnShutDown(List<string> options)
        {
            Show("Good Bye!");
            Environment.Exit(0);
        }

        private void OnHelp(List<string> options)
        {
            Show("You requested help");
        }

        private void RegisterCommands()
        {
            Commands.Register("request", OnChallengeRequest);
            Commands.Register("resolve", OnSolution);
            Commands.Register("shutdown", OnShutDown);
            Commands.Register("help", OnHelp);
        }

        private void Show(string message)
        {
            Console.WriteLine(message);
        }
        public void ListenForCommands()
        {
            if (!Commands.Run(Console.ReadLine()))
            {
                Show("Invalid Command");
            }
            ListenForCommands();
        }


    }
}
