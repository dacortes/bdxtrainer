﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BDXTrainer
{
    public class TrainingCommands
    {
        private readonly Dictionary<string, Action<List<string>>> commands;

        public TrainingCommands()
        {
            commands = new Dictionary<string, Action<List<string>>>();
        }

        public void Register(string commandName, Action<List<string>> commandAction)
        {
            commands.Add(commandName, commandAction);
        }

        public bool Run(string line)
        {
            var commandName = line.Split(' ')[0];
            line = Regex.Replace(line, "^"+commandName+"( +)?", string.Empty);
            if (!commands.ContainsKey(commandName)) return false;
            var options = line.Split(' ').ToList().Select(o=>o.Trim()).ToList();
            commands[commandName].Invoke(options);
            return true;
        }

        
    }
}
